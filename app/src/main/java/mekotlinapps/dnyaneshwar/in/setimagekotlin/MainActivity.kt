package mekotlinapps.dnyaneshwar.`in`.setimagekotlin

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {

    val CAMERA_PERMISSIONS = 101
    val REQUEST_FOR_CAMERA = 102
    val REQUEST_FOR_GALLERY = 103

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermssion()
            } else {
                getImage()
            }
        }
    }


    fun requestPermssion() {
        ActivityCompat.requestPermissions(this, arrayOf("android.permission.CAMERA", "android.permission.READ_EXTERNAL_STORAGE"), CAMERA_PERMISSIONS);
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        when (requestCode) {
            CAMERA_PERMISSIONS -> if (grantResults.size > 0) {

                var cameraPermission = grantResults[0] === PackageManager.PERMISSION_GRANTED
                var readStoragePermssion = grantResults[1] === PackageManager.PERMISSION_GRANTED

                if (cameraPermission && readStoragePermssion) {
                    getImage()
                } else {
                    alert("Please grant all permissoins to set image.") {
                        negativeButton("RETRY") { requestPermssion() }
                    }.show()
                }

            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    fun getImage() {
        val itemList = arrayOf<CharSequence>("TAKE PHOTO", "CHOOSE FROM GALLERY", "CANCEL")
        val builder = AlertDialog.Builder(this)

        builder.setItems(itemList, object : DialogInterface.OnClickListener {
            override fun onClick(dialog: DialogInterface?, which: Int) {
                if (itemList[which] == "TAKE PHOTO") {
                    getfromCamera()
                    dialog!!.dismiss()
                } else if (itemList[which] == "CHOOSE FROM GALLERY") {
                    getfromGallery()
                    dialog!!.dismiss()
                } else {
                    dialog!!.dismiss()
                }
            }
        });
        builder.show()
    }

    fun getfromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_FOR_CAMERA)
    }

    fun getfromGallery() {
        val intent = Intent()
        intent.setType("image/*")
        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), REQUEST_FOR_GALLERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {

                REQUEST_FOR_CAMERA -> {
                    imageview.setImageURI(data!!.data)
                }
                REQUEST_FOR_GALLERY -> {
                    imageview.setImageURI(data!!.data)
                }

            }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
